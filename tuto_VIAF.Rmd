---
title: 'Tutoriel alignement de vocabulaire : VIAF'
author: "Lizzie Scholtus"
date: "`r format(Sys.time(), '%d %B %Y')`"
output: 
  bookdown::html_document2 :
    toc: yes
    toc_float: yes
    number_sections: yes
    theme: united
    fig_caption: yes
    self_contained: true
---


```{r klippy, echo=FALSE, include=TRUE}
klippy::klippy(position = c('top', 'right'), tooltip_message = 'Copier le code')
```

<style>
p.caption {
  font-size: 0.9em;
  font-style: italic;
  padding-top: 1em;
}

#TOC::before {
  content: "";
  display: block;
  height: 100px;
  margin: 10px 10px 20px 10px;
  background-image: url("masa.png");
  background-size: contain;
  background-position: center center;
  background-repeat: no-repeat;
}

</style>


```{r setup, include=FALSE}
knitr::opts_chunk$set(eval = TRUE, echo = FALSE)
```

```{r library, eval=TRUE, include=FALSE}
library(bookdown)
```

# Avant-propos {-}
<br>

<div style="text-align: justify">
Ce tutoriel a pour objectif d'expliquer comment effectuer un alignement de vocabulaire vers le référentiel [VIAF](http://viaf.org/)(Virtual International Authority File), à l'aide du logiciel libre [OpenRefine](https://openrefine.org/)^[Pour voir comment installer et utiliser OpenRefine lire ce [tutoriel](https://msaby.gitlab.io/tutoriel-openrefine/)]. Le but de cet alignement est de relier un jeu de données archéologiques contenant des acteurs au VIAF afin de pouvoir utiliser le websémantique et en particulier l'outil [OpenArchaeo](http://openarchaeo.huma-num.fr/explorateur/home). Ce document est établi à l'aide des données provenant de la base [RITA](https://rita.huma-num.fr/s/rita/page/accueil) qui se présente au format csv.
</div>

<br>

<div style="font-size: 09pt">
**Aspects techniques**  
<div style="text-align:justify">
Ce tutoriel est écrit en R Markdown à l'aide du package "Bookdown" créé par Yihui Xie (*bookdown: Authoring Books and Technical Documents with R Markdown*. R package version 0.24., 2021).
</div>
</div>

<br>

-----

<br>

# Récupérer et préparer les noms propres à aligner {#prep}
<br>

<div style="text-align: justify">
Selon l'architecture du fichier de données, il est d'abord nécessaire d'isoler les noms propres sur lesquels l'alignement doit être réalisé au sein d'une même colonne nommée **Noms**^[Pour les cas où les fichiers sont les moins structurés et où ces noms se trouvent au sein de phrases explicatives plutôt que dans des champs systématiques, il est obligatoire d'effectuer un nettoyage des données au préalable en divisant les phrases en fonction des espaces et en utilisant les facettes et regroupements proposés par OpenRefine comme cela est expliqué dans la [partie 5](https://msaby.gitlab.io/tutoriel-openrefine/exploration-et-nettoyage-de-base.html#regrouper-des-valeurs-proches) du tutoriel précédemment cité.].  

Afin de réaliser une réconciliation (partie \@ref(rec)) la plus optimale possible, il est nécessaire d'effectuer certaines opérations de mise en forme &ndash; ou nettoyage &ndash; sur ces noms propres. Afin de préserver l'organisation des données, mais aussi les noms sources, il est préférable d'effectuer ces modifications au sein d'une nouvelle colonne **noms_source** dans laquelle les informations de la colonne **Noms** auront été dupliquées (figure \@ref(fig:dupliquer)).
</div>

<br>

(ref:dupliquer) Dupliquer la colonne **Noms**.


```{r dupliquer, fig.cap="(ref:dupliquer)", fig.show = "hold", fig.align = "center"}
 knitr::include_graphics("dupliquer.png")
 knitr::include_graphics("dupliquer_2.png")
```

<br>

<div style="text-align: justify">
Si aucune opération de nettoyage et de normalisation n'a été effectuée auparavant, il est possible qu'il y ait des problèmes d'homogénéisation dans la manière de présenter ces noms ainsi que des fautes de frappe qui doivent être résolus avant de poursuivre.  

Ensuite, parce que les étapes suivantes peuvent être très longues selon la taille du fichier à aligner, il est préférable de supprimer tous les noms en double dans la colonne **noms_source**. Un tri est donc effectué sur cette colonne, puis validé en utilisant l'option "Retrier les lignes de façon permanente" accessible dans l'onglet **Trier** au-dessus du tableur (figure \@ref(fig:tri)). Enfin, l'option "Vider les valeurs répétées dans des cellules consécutives" permet de supprimer tous les doublons (figure \@ref(fig:doublon)).
</div>

<br>

(ref:tri) Trier les données de la colonne **noms_source** avant d'en supprimer les doublons.

```{r tri, fig.cap="(ref:tri)", fig.show = "hold", fig.align = "center"}
 knitr::include_graphics("tri_1.png")
 knitr::include_graphics("tri_2.png")
```

(ref:doublon) Supprimer les doublons dans la colonne **noms_source**.

```{r doublon, fig.cap="(ref:doublon)", out.width = "40%", fig.align = "center"}
 knitr::include_graphics("doublon.png")
```

<br>

# Réconcilier les noms avec le référentiel VIAF {#rec}
<br>

<div style="text-align: justify">
La réconciliation des données permet de rapprocher chaque valeur du jeu de données source vers de possibles candidats d'un vocabulaire contrôlé. Elle permet par la suite d'enrichir les données sources en important de nouvelles informations depuis ce vocabulaire de référence, telles que son identifiant, sa définition, son *preflabel* ou tout autre élément jugé utile^[Voir [manuel Openrefine](https://docs.openrefine.org/manual/reconciling) (en anglais)]. Il s'agit donc d'un alignement de vocabulaire qui repose sur un service spécialement conçu pour OpenRefine^[[Saby 2020](https://msaby.gitlab.io/tutoriel-openrefine/enrichir-ses-donn%C3%A9es-et-aligner-sur-des-sources-ext%C3%A9rieures.html#r%C3%A9concilier-des-donn%C3%A9es-avec-wikidata)].  
</div>

<br>

## Ajouter un service de réconciliation
<br>

<div style="text-align: justify">
La première étape consiste à installer le service de réconciliation du VIAF dans OpenRefine. Pour ce faire, on sélectionne la colonne à aligner &ndash; en l'occurrence **noms_source** &ndash; et l'option **"Démarrer la réconciliation"** dans **"Réconcilier"** (figure \@ref(fig:rec1)). Lors d'une première utilisation, seul le service de réconciliation de Wikidata est installé. Il faut donc ajouter le service du VIAF en cliquant sur le bouton **"Ajouter un service standard"** (figure \@ref(fig:rec2)) puis indiquer l'adresse du service de réconciliation voulu (figure \@ref(fig:rec3)).  

Il n'existe pas de service de réconciliation officiel pour le VIAF, mais plusieurs utilisateurs en ont créé et les laissent à la disponibilité de la communauté. Il est donc tout à fait possible de s'en servir, tout en gardant à l'esprit qu'ils ne permettent pas forcément d'utiliser l'ensemble des fonctions du service de réconciliation et qu'ils peuvent ne pas être aussi stables et puissants qu'un service dédié. Parmi ces services on peut citer :

+   celui créé par Jeff Chiu^[Lire la [documentation](http://refine.codefork.com/).].

````markdown
https://github.com/OpenRefine/OpenRefine/wiki/Reconcilable-Data-Sources
````

+   ou encore celui créé par Roderic D. M. Page^[Lire la [documentation](https://iphylo.blogspot.com/2013/04/reconciling-author-names-using-open.html).].

````markdown
http://iphylo.org/~rpage/phyloinformatics/services/reconciliation_viaf.php
````
<br>

(ref:rec1) Ouvrir le service de réconciliation.

```{r rec1, fig.cap="(ref:rec1)", out.width = "40%", fig.align = "center"}
 knitr::include_graphics("rec_1.png")
```

(ref:rec2) Ajouter un service de réconciliation.

```{r rec2, fig.cap="(ref:rec2)", out.width = "90%", fig.align = "center"}
 knitr::include_graphics("rec_2.png")
```

(ref:rec3) Indiquer l'adresse du service.

```{r rec3, fig.cap="(ref:rec3)", out.width = "60%", fig.align = "center"}
 knitr::include_graphics("rec_3.png")
```

<br> 

Le lien avec le référentiel du VIAF est désormais installé dans l'ordinateur et peut être sélectionné dans l'espace à gauche de la fenêtre (figure \@ref(fig:rec4)).  
</div>

<br>

(ref:rec4) Le service de réconciliation du VIAF est désormais disponible dans OpenRefine.

```{r rec4, fig.cap="(ref:rec4)", out.width = "100%", fig.align = "center"}
 knitr::include_graphics("rec_4.png")
```

<br>

## Lancer la réconciliation
<br>

<div style="text-align: justify">
La réconciliation avec le VIAF peut se faire d'après différents types d'informations &ndash; personne, organisation, ouvrage, édition &ndash; que l'on peut sélectionner avant de démarrer l'opération (figure \@ref(fig:rec5) rectangle orange clair). Toutefois, dans l'exemple développé ici, la colonne **noms_source** contient à la fois des noms de personnes et des noms d'institutions, c'est pourquoi la mention "Réconcilier sans type particulier" est privilégiée (figure \@ref(fig:rec5) rectangle orange foncé).

<br>

(ref:rec5) Sélectionner le type d'information recherché.

```{r rec5, fig.cap="(ref:rec5)", out.width = "100%", fig.align = "center"}
 knitr::include_graphics("rec_5.png")
```

<br>

La réconciliation peut aussi être améliorée en croisant d'autres informations relatives au nom recherché. Cette possibilité est particulièrement utile pour différencier des homonymes. Ainsi, la partie gauche de la fenêtre de réconciliation (figure \@ref(fig:rec6)) permet de sélectionner dans le jeu de données source des colonnes apportant des informations complémentaires, qui correspondent aussi à des champs existant dans le référentiel VIAF. On privilégiera plutôt des éléments comme les dates de naissance et de décès.

<br>

(ref:rec6) Ajouter des informations complémentaires pour différencier les homonymes.

```{r rec6, fig.cap="(ref:rec6)", out.width = "100%", fig.align = "center"}
 knitr::include_graphics("rec_6.png")
```

<br>

Openrefine propose aussi d'aligner automatiquement les termes pour lesquels il détecte un haut pourcentage de correspondance en cochant l'option **"Correspondance automatique des valeurs candidates"** (figure \@ref(fig:rec6)). Cette fonctionnalité est à utiliser avec précaution et donne des résultats plus ou moins bons selon les référentiels recherchés. Dans la plupart des cas il vaut mieux vérifier manuellement les résultats obtenus pour en certifier la concordance.  

Une fois ces éléments indiqués, il est possible de démarrer la réconciliation. Cette opération peut prendre un certain temps &ndash; d'autant plus qu'il ne s'agit pas de services créés directement par le VIAF, mais par des utilisateurs indépendant &ndash;, c'est pourquoi il était important de supprimer les éventuels doublons au début de ce tutoriel (partie \@ref(prep)). De la même manière, dans le cas de jeux de données conséquents, il est préférable de les diviser.
</div>
<br>

## Valider la réconciliation
<br>

<div style="text-align: justify">
Le résultat de la réconciliation consiste en une liste de possibles correspondances pour chaque nom (figure \@ref(fig:rec7)). Bien entendu, il arrive qu'aucun alignement ne puisse être établi avec le référentiel cible. On remarque que le tableur en lui-même n'a pas été modifié et qu'il n'existe toujours qu'une cellule pour chaque nom. Par ailleurs, le contenu des cellules n'est pas réellement modifié non plus, elles contiennent uniquement le nom.

<br>

(ref:rec7) Résultat de la réconciliation

```{r rec7, fig.cap="(ref:rec7)", out.width = "80%", fig.align = "center"}
 knitr::include_graphics("rec_7.png")
```

<br>

Cette liste de possibles concordances se compose en réalité d'hyperliens vers le référentiel. Cliquer dessus ouvre la page du VIAF lui correspondant et permet de voir les différentes informations concernant la personne en question : dates de naissance et de mort, occupation, publication, nationalité, etc. (figure \@ref(fig:rec8)). Si le nom analysé correspond, il suffit de cocher l'option **"Apparier cette cellule"** à côté du nom pour valider sa réconciliation. Si le même nom, désignant exactement la même personne, peut se répéter dans le jeu de données, il est aussi possible de cocher l'option **"Apparier toutes les cellules identiques"** pour qu'il soit aligné dans l'ensemble du document. Cette opération est à refaire pour chaque terme^[Puisqu'elle nécessite de bien connaître les données sources et les personnes ou institutions qu'elles désignent pour pouvoir les comparer au référentiel cible, il est préférable que cette opération soit réalisée &ndash; ou vérifiée par la suite &ndash; par quelqu'un qui maîtrise les données analysées.].

<br>

(ref:rec8) Exemple d'une fiche du VIAF et des informations qu'elle propose

```{r rec8, fig.cap="(ref:rec8)", out.width = "80%", fig.align = "center"}
 knitr::include_graphics("viaf.png")
```

<br>

Le VIAF est un appariement et un établissement de liens entre les fichiers d'autorité de bibliothèques nationales. Il n'est pas spécifique à l'archéologie et ne se veut pas exhaustif. Il est donc évident qu'un certain nombre de noms ne pourra être aligné^[Pour les plus récents d'entre eux, il est aussi possible de faire des alignements avec [ORCID](https://orcid.org/) ou avec [IdRef](https://www.idref.fr/).].
</div>
<br>

# Enrichir les données avec les informations des VIAF
<br>

<div style="text-align: justify">
La réconciliation des données avec un référentiel externe permet aussi de récupérer des informations présentes dans ce référentiel et de les ajouter aux données sources. Cette opération s'appelle un enrichissement des données. Puisque la réconciliation effectuée ici a pour objectif d'aligner les noms présents dans le jeu de données source avec le VIAF, nous avons besoin de récupérer les identifiants pérennes de ce référentiel. Toutefois, pour que cet alignement puisse être validé, un certain nombre d'autres informations peut également être extrait afin de déterminer plus facilement les termes sources et les termes cibles correspondant.  

Avant d'importer ces diverses informations, il peut être utile de récupérer la liste de références possibles proposées par la réconciliation en la sauvegardant dans une nouvelle colonne **possibilites**, afin de pouvoir toujours revérifier l'alignement. La colonne **noms_source** est donc dupliquée à l'aide de la fonction "Ajouter une colonne en fonction de cette colonne", puis les données sont modifiées à l'aide d'une expression GREL^[L'acronyme GREL correspond à *General Refine Expression Language*, un langage d'expression développé d'après JavaScript pour OpenRefine. Pour plus d'informations voir le [manuel](https://docs.openrefine.org/manual/grel) d'OpenRefine] permettant de récupérer pour chaque cellule les candidats possibles à l'alignement (figure \@ref(fig:possible)).

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
forEach(cell.recon.candidates,c,c.name).join(", ")
````
<br>

(ref:possible) Dupliquer la colonne **noms_source** pour récupérer la liste des alignements possibles.

```{r possible, fig.cap="(ref:possible)", fig.show = "hold", fig.align = "center"}
 knitr::include_graphics("possible_1.png")
 knitr::include_graphics("possible_2.png")
```

<br>

## Moissonner des URL
<br>

Le service de réconciliation utilisé ici ne permet pas d'accéder directement au VIAF et aux informations qu'il contient. Par contre, il est possible de créer une nouvelle colonne **ID_VIAF** contenant les identifiants pérennes de ce référentiel (figure \@ref(fig:ID)) et de s'en servir ensuite pour reconstituer &ndash; toujours à l'aide d'une expression GREL &ndash; les URL menant vers les fiches d'informations à moissonner^[Pour plus d'informations sur la manière de moissonner le VIAF et les informations accessibles, voir le site de [OCLC](https://www.oclc.org/developer/develop/web-services/viaf/authority-cluster.en.html).]. Cette deuxième partie s'effectue grâce à la fonction "Ajouter une colonne en moissonnant des URL" d'Openrefine (figure \@ref(fig:moisson)) qui nous permet de créer la colonne **VIAF** contenant toutes les informations du référentiel au format JSON.
</div>

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
"http://viaf.org/viaf/" + value + "/viaf.json"
````
<br>

(ref:ID) Créer une nouvelle colonne contenant les identifiants des ressources alignées.

```{r ID, fig.cap="(ref:ID)", out.width = "40%", fig.align = "center"}
 knitr::include_graphics("ID.png")
```

<br>

(ref:moisson) Moissonner le contenu d'une fiche VIAF d'après une URL.

```{r moisson, fig.cap="(ref:moisson)", fig.show = "hold", fig.align = "center"}
 knitr::include_graphics("moisson.png")
 knitr::include_graphics("moisson_2.png")
```

<br>

## Nettoyer et mettre en forme l'import VIAF
<br>

<div style="text-align: justify">
Il est alors possible d'extraire des données moissonnées toutes les informations disponibles et nécessaires à la validation des alignement en dupliquant la colonne **VIAF** et en y appliquant différentes expressions GREL.

+   Récupérer les **URI**

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
value.parseJson().Document['@about']
````
<br>

+   Récupérer la **date de naissance**

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
value.parseJson().birthDate
````
<br>

+   Récupérer la **date de mort**

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
value.parseJson().deathDate
````
<br>

+   Récupérer les nationalités (**nationalite**)

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
value.parseJson().nationalityOfEntity.data.text
````
<br>

+   Récupérer les données contenues dans le champ activités : pour cet élément, il faut fonctionner en deux temps. On récupère d'abord les données des activités multiples en copiant la colonne **VIAF** dans une nouvelle colonne **activite** que l'on filtre sur le texte et dont on extrait uniquement le texte à l'aide d'une transformation de cellules (figure \@ref(fig:transfo)).

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
<!-- filtrer le texte sur le champ activité --> 
value.parseJson()['fieldOfActivity']

<!-- Ne conserver que le texte -->
forEach(value.parseJson().data, v, v.text).join(" ; ")
````

<br>

(ref:transfo) Appliquer une transformation sur l'ensemble des cellules d'une même colonne.

```{r transfo, fig.cap="(ref:transfo)", out.width = "40%", fig.align = "center"}
 knitr::include_graphics("transfo.png")
```

<br>

Toutefois, les entités pour lesquelles une seule activité est renseignée dans ce champ ne sont pas mise au propre par cette dernière opération. Pour ce faire, il est ensuite nécessaire de les sélectionner à l'aide d'une facette textuelle personnalisée (figure \@ref(fig:facette)) puis d'y appliquer une transformation.

<span style="color: #e67e22;">**Expression GREL (pour la facette) :**</span>   
````markdown
value.startsWith("{")
````

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
value.parseJson().data.text
````

<br>

(ref:facette) Sélectionner une partie des cellules à l'aide d'une facette personnalisée et y appliquer une transformation.

```{r facette, fig.cap="(ref:facette)", fig.show = "hold", fig.align = "center"}
 knitr::include_graphics("facette_1.png")
 knitr::include_graphics("facette_3.png")
 knitr::include_graphics("facette_2.png")
```

<br>

+   Récupérer les **titres** des publications liées à l'entité^[Dans l'expression GREL proposée, les différents types d'œuvres sont séparés par un `|` qui peut aussi être utilisé pour diviser la cellule et ainsi créer une nouvelle entrée dans laquelle chaque ligne indiquera le titre d'une seule ligne. Bien que plus lisible pour la colonne **titres**, cette manipulation peut rendre le lien entre le nom source et la proposition d'alignement plus compliqué à conserver dans le tableur.]

<span style="color: #e67e22;">**Expression GREL :**</span>   
````markdown
forEach(value.parseJson().titles.work, v, v.title).join(" | ")
````

<br>

Il est désormais possible de supprimer la colonne **VIAF** qui n'aura plus d'utilité et de ré-agencer les autres pour rendre le document plus lisible et permettre aux chercheurs ou aux personnes en charge des données de vérifier la validité de cet alignement.
</div>

<br>

# Ressources et références {-}
<br>

<div style="text-align: justify">
+   Blaney, Jonathan. « Introduction to the Principles of Linked Open Data ». *Programming Historian*, mai 2017, https://programminghistorian.org/en/lessons/intro-to-linked-data.

+   Hooland, Seth van, et al. « Nettoyer ses données avec OpenRefine ». *Programming Historian*, août 2013, https://programminghistorian.org/fr/lecons/nettoyer-ses-donnees-avec-openrefine.

+   *IdRef - Identifiants et référentiels pour l’Enseignement supérieur et la Recherche*. https://www.idref.fr/. Consulté le 4 novembre 2021.

+   *iPhylo: Reconciling author names using Open Refine and VIAF*. https://iphylo.blogspot.com/2013/04/reconciling-author-names-using-open.html. Consulté le 4 novembre 2021.

+   *OpenRefine User Manual | OpenRefine*. https://docs.openrefine.org//. Consulté le 8 octobre 2021.

+   *ORCID*. https://orcid.org/. Consulté le 4 novembre 2021.

+   *Reconciliation Services for OpenRefine*. http://refine.codefork.com/. Consulté le 4 novembre 2021.

+   Saby, Mathieu. *Tutoriel OpenRefine 3.4 : nettoyer, préparer et transformer des données - 06/11/2020*. https://msaby.gitlab.io/tutoriel-openrefine. Consulté le 8 octobre 2021.

+   *VIAF*. http://viaf.org/. Consulté le 4 novembre 2021.

+   « VIAF (Virtual International Authority File) | OCLC Developer Network ». *OCLC*, 3 novembre 2021, https://www.oclc.org/developer/api/oclc-apis/viaf.en.html.

+   Williamson, Evan Peter. « Fetching and Parsing Data from the Web with OpenRefine ». *Programming Historian*, août 2017, https://programminghistorian.org/en/lessons/fetch-and-parse-data-with-openrefine.
</div>


<br>
<br>
<br>



