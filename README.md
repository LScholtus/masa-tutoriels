![](https://masa.hypotheses.org/files/2022/04/bandeau_MASA_2022-2.jpg)

Ce dépôt contient un ensemble de tutoriels créé pour le consortium [MASA](https://masa.hypotheses.org/) de l'IR [Huma-Num](https://www.huma-num.fr/) pour faciliter et diffuser la science ouverte et les principes FAIR dans le domaine de l'archéologie. Ces tutoriels ont été créé dans le cadre de la rédaction du [livre blanc](https://shs.hal.science/halshs-03561376v2) de MASA.  

[Tutoriel AAT](https://lscholtus.gitlab.io/masa-tutoriels/AAT.html) : Alignement de données vers le référentiel de vocabulaire du getty museum.  
[Tutoriel geonames](https://lscholtus.gitlab.io/masa-tutoriels/Tuto_geonames.html) : Alignement de données vers le référentiel de lieu de geonames.  
[Tutoriel PACTOLS](https://lscholtus.gitlab.io/masa-tutoriels/tuto_pactols.html) : Alignement de données vers le référentiel de vocabulaire PACTOLS.  
[Tutoriel VIAF](https://lscholtus.gitlab.io/masa-tutoriels/tuto_VIAF.html) : Alignement de données vers le référentiel de personnes VIAF.


